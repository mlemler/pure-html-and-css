# pure HTML and CSS

This is the first version of my new homepage purely written in HTML and CSS. Maybe other version will follow with more
advanced technologies but more or less copying this design.

## Used techologies

### HTML
* *Semantic HTML5 Elements* - Mostly basics but maybe I can mention that I tried to use the semantic HTML5 elements like
`<header>`, `<main>`, `<aside>` and `<footer>`.
* *Viewport Meta Tag* - In my first version I forgot the viewport meta tag and thus my page did not show up as mobile
friendly as I thought because mobile browser often try to scale down the page to match the screen instead of using their
own screen size.

### CSS
* *Property Ordering* - This is not a technology but a convention. It is good practice to order the CSS properties by *name*
and not by semantics, appearance or anything else. This is the easiest way anyone will find the property he is searching
for or to see if one is missing.
* *Box Sizing* - For easier calculation of box sizes I used `border-box` globally by setting it on the `body` Element and
inheriting it for all children.
* *Flexbox* - For most layout parts I used Flexbox, and I love it.
* *Grid* - I also tried a bit of CSS Grid for layouting as well. I am getting better using it.
* *Media Queries* - As my side is quite small there is no big need for mobile improvement but at least I put the *aside*
content below the main content on smaller screens to improve the readability by using a media query.

### Images
* *Image Optimization* - This is a real big topic but I just wanted to mention two thing. First of all you should resize
the images you use to the maximum showcase. So if you show an image just like an avatar (like on my page) there is no need
for thousands of pixels. My image is still to big but you should have seen the original one ;-) And secondary there are tool like *ImageOptim* or *TinyPNG* which optimize you images for the usage
on a website by compressing them and removing unneeded data.

## Links

### HTML5 Elements
* [HTML Content Sectioning (MDN)](https://developer.mozilla.org/en-US/docs/Web/HTML/Element#content_sectioning)

### Viewport Tag
* [Viewport meta tag (MDN)](https://developer.mozilla.org/en-US/docs/Web/HTML/Viewport_meta_tag)

### Box Sizing
* [Box sizing (MDN)](https://developer.mozilla.org/en-US/docs/Web/CSS/box-sizing)

### Flexbox
* [Flexbox (MDN)](https://developer.mozilla.org/en-US/docs/Glossary/Flexbox)
* [A Compplete Guide to Flexbox (CSS-Tricks)](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
* [Flexbox Froggy](http://flexboxfroggy.com)

### Grid
* [Grid (MDN)](https://developer.mozilla.org/en-US/docs/Glossary/Grid)
* [A Complete Guide to Grid (CSS-Tricks)](https://css-tricks.com/snippets/css/complete-guide-grid/)
* [Grid Garden](http://cssgridgarden.com)

### Media Queries
* [Using media queries (MDN)](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries)

### Image Optimization
* [ImageOptim](https://imageoptim.com)
* [TinyPNG](https://tinypng.com/)
